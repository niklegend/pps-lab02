package u02lab.code;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class BaseSequenceGenerator  implements SequenceGenerator{

    int counter;
    private final int stop;

    BaseSequenceGenerator(int n){
        counter = 0;
        stop = n;
    }

    public void reset(){
        counter = 0;
    }

    public boolean isOver() {
        return counter == stop;
    }

    public List<Integer> allRemaining() {
        return IntStream.range(counter, stop)
                .boxed()
                .collect(Collectors.toList());
    }

    public Optional<Integer> next() {
        if (!isOver()) {
            return generateElement();
        }
        return Optional.empty();
    }

    protected abstract Optional<Integer> generateElement();
}
