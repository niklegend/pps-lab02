package u02lab.code.factories;

import u02lab.code.RandomGenerator;
import u02lab.code.RangeGenerator;
import u02lab.code.SequenceGenerator;

public class DefaultGeneratorFactory implements GeneratorFactory {
    @Override
    public SequenceGenerator createRandomGenerator(int sequenceSize) {
        return new RandomGenerator(sequenceSize);
    }

    @Override
    public SequenceGenerator createRangeGenerator(int initialNumber, int finalNumber) {
        return new RangeGenerator(initialNumber, finalNumber);
    }
}
