package u02lab.code.factories;

import u02lab.code.SequenceGenerator;

public interface GeneratorFactory {
    SequenceGenerator createRangeGenerator(int finalNumber, int initialNumber);
    SequenceGenerator createRandomGenerator(int sequenceSize);
}
