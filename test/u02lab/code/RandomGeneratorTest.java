package u02lab.code;

import org.junit.Before;
import org.junit.Test;
import u02lab.code.factories.DefaultGeneratorFactory;
import u02lab.code.factories.GeneratorFactory;

import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class RandomGeneratorTest {

    private static final int RANDOM_BITS_NUMBER = 5;
    private static final int NEXT_CALLED_NUMBER = 2;

    private SequenceGenerator sequenceGenerator;

    @Before
    public void setUp() {
        GeneratorFactory generatorFactory = new DefaultGeneratorFactory();
        sequenceGenerator = generatorFactory.createRandomGenerator(RANDOM_BITS_NUMBER);
    }

    @Test
    public void next() {
        IntStream.range(0, RANDOM_BITS_NUMBER)
                .map(n -> sequenceGenerator.next().get())
                .forEach(bit -> assertTrue(isBit(bit)));
    }

    private boolean isBit(Integer bit) {
        return bit.equals(0) || bit.equals(1);
    }

    @Test
    public void reset() {
        callNext();
        sequenceGenerator.reset();
        assertEquals(sequenceGenerator.allRemaining().size(), RANDOM_BITS_NUMBER);
    }

    @Test
    public void isOverInMiddle() {
        callNext();
        assertFalse(sequenceGenerator.isOver());
    }

    @Test
    public void isOverInTheEnd(){
        IntStream.range(0, RANDOM_BITS_NUMBER)
                .forEach(i -> sequenceGenerator.next());
        assertTrue(sequenceGenerator.isOver());
    }

    @Test
    public void allRemaining() {
        callNext();
        assertEquals(sequenceGenerator.allRemaining().size(),
                RANDOM_BITS_NUMBER - NEXT_CALLED_NUMBER);
    }

    private void callNext() {
        IntStream.range(0, NEXT_CALLED_NUMBER)
                .forEach(n -> sequenceGenerator.next());
    }
}
