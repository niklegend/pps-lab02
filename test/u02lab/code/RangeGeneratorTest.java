package u02lab.code;

import org.junit.Before;
import org.junit.Test;
import u02lab.code.factories.DefaultGeneratorFactory;
import u02lab.code.factories.GeneratorFactory;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class RangeGeneratorTest {

    private static final int START_NUMBER = 0;
    private static final int END_NUMBER = 10;

    private SequenceGenerator sequenceGenerator;

    @Before
    public void setUp() {
        GeneratorFactory generatorFactory = new DefaultGeneratorFactory();
        sequenceGenerator = generatorFactory.createRangeGenerator(START_NUMBER, END_NUMBER);
    }

    @Test
    public void testNext() {
        IntStream.range(START_NUMBER, END_NUMBER)
            .forEach(number -> assertEquals(Integer.valueOf(number),
                    sequenceGenerator.next().get()));
    }

    @Test
    public void firstNext(){
        assertEquals(Integer.valueOf(START_NUMBER),
                sequenceGenerator.next().get());
    }

    @Test
    public void nextAfterStop(){
        IntStream.range(START_NUMBER, END_NUMBER)
                .forEach(n -> sequenceGenerator.next());
        assertEquals(Optional.empty(), sequenceGenerator.next());
    }

    @Test
    public void reset() {
        sequenceGenerator.next();
        sequenceGenerator.reset();
        assertEquals(Integer.valueOf(START_NUMBER), sequenceGenerator.next().get());
    }

    @Test
    public void isOverInMiddle() {
        sequenceGenerator.next();
        assertFalse(sequenceGenerator.isOver());
    }

    @Test
    public void isOverInTheEnd(){
        IntStream.range(START_NUMBER, END_NUMBER)
                .forEach(n -> sequenceGenerator.next());
        assertTrue(sequenceGenerator.isOver());
    }

    @Test
    public void allRemaining() {
        sequenceGenerator.next();
        sequenceGenerator.next();
        assertEquals(Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9),
                sequenceGenerator.allRemaining());
    }
}
